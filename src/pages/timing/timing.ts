import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Storage} from "@ionic/storage";

/**
 * Generated class for the TimingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-timing',
  templateUrl: 'timing.html',
})
export class TimingPage {

  type : string = 'client';
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage : Storage) {
    storage.get('type').then((val) => {
      this.type = val;
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TimingPage');
  }

}
