import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Storage} from "@ionic/storage";

/**
 * Generated class for the ConfirmationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirmation',
  templateUrl: 'confirmation.html',
})
export class ConfirmationPage {

  type : string = 'client';
  appointment : any = {};
  client : any = {};
  stylist : any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private storage : Storage) {
    storage.get('type').then((val) => {
      this.type = val;
      if(this.type=='stylist'){

        this.client = this.navParams.get('client');
        this.appointment = this.navParams.get('request');
      } else {
        this.stylist = this.navParams.get('stylist');
        this.appointment = this.navParams.get('request');
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmationPage');
  }
  gotoBack() {
    this.navCtrl.pop();
  }

  pushToFind() {
    this.storage.set("pop","ok");
    this.navCtrl.pop();
  }
}
