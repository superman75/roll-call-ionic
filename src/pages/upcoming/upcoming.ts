import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {AngularFireDatabase} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";
import {ToastProvider} from "../../providers/toast/toast";

/**
 * Generated class for the UpcomingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-upcoming',
  templateUrl: 'upcoming.html',
})
export class UpcomingPage {
  reservation_list = [];
  timeArray : string[] = [
    '12:00 AM - 2:00 PM',
    '2:00 PM - 4:00 PM',
    '4:00 PM - 6:00 PM',
    '6:00 PM - 8:00 PM'
  ];
  currentCursor = 0;

  stylists : any = [];
  objectSubscription : any;
  objectSubscription1 : any;
  currentUserId : string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private afAuth : AngularFireAuth,
              private toastProvider : ToastProvider,
              private alertCtrl : AlertController,
              private afDB : AngularFireDatabase) {
    this.currentCursor = 0;
    this.afAuth.authState.take(1).subscribe( data=>{
      this.currentUserId = data.uid;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpcomingPage');
  }

  ionViewWillEnter() {
    this.objectSubscription = this.afDB.object('/appointments').valueChanges()
      .subscribe(snapShots=>{
        if (!snapShots) return;
        let results = Object.keys(snapShots);
        this.reservation_list = [];
        results.forEach((key,index)=> {
          let appointment = snapShots[key];
          if(appointment.status >0 && appointment.status<4 && appointment.clientId == this.currentUserId) {
            this.reservation_list.push(appointment);
          }
        });
      });
    this.objectSubscription1 = this.afDB.object('/users').valueChanges()
      .subscribe(snapShots=>{
        if (!snapShots) return;
        let results = Object.keys(snapShots);

        this.stylists = [];
        results.forEach((key,index)=> {
          let user = snapShots[key];
          if(user.type == 'stylist') {
            this.stylists.push(user);
          }
        });
      })
  }

  ionViewWillLeave() {
    this.objectSubscription.unsubscribe();
    this.objectSubscription1.unsubscribe();
  }


  changeCursor(i : number) {
    this.currentCursor = i;
  }

  openThread() {
    console.log('Open Chat');
    this.navCtrl.push('ThreadPage');
  }

  openRule() {
    console.log("open Rule");
    this.navCtrl.push('RulePage');
  }

  cancelRequest() {
    console.log("cancel Rule");
  }

  checkin(stylist, reservation) {
    console.log('check in');
    this.navCtrl.push('CheckInPage',{'stylist' : stylist, 'reservation' : reservation});
  }

  skipStep() {
    console.log('skip step');
  }
}
