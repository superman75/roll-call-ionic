import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Storage} from "@ionic/storage";

/**
 * Generated class for the CheckInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-check-in',
  templateUrl: 'check-in.html',
})
export class CheckInPage {

  type :string = 'client';
  stylist : any = {};
  reservation : any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private storage : Storage) {
    this.storage.get('type').then((val) => {
      this.type = val;
    });
    this.stylist = this.navParams.get('stylist');
    this.reservation = this.navParams.get('reservation');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckInPage');
  }
  gotoBack() {
    this.navCtrl.pop();
  }

}
