import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {SplashScreen} from "@ionic-native/splash-screen";
import {Storage} from "@ionic/storage";
import {Geolocation} from "@ionic-native/geolocation";

/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  type : string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public splash : SplashScreen,
              private storage : Storage,
              private geolocation  :Geolocation) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
    this.splash.hide();

    this.geolocation.getCurrentPosition().then((resp) => {
      console.log('lat : ' + resp.coords.latitude + ' , long : ' +  resp.coords.longitude);
    }).catch((error) => {
      console.log('Error getting location', error.message);
    });



  }

  changeType(type: string) {
    this.type = type;
    this.storage.set('type', type);
  }

  pushToLogin() {
    this.navCtrl.setRoot('LoginPage');
  }

}
