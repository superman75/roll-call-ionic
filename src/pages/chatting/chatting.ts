import {Component, ViewChild} from '@angular/core';
import {Content, IonicPage, Keyboard, NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";

/**
 * Generated class for the ChattingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-chatting',
  templateUrl: 'chatting.html',
})
export class ChattingPage {

  @ViewChild(Content) content: Content;
  type : string = '';
  message : string = '';
  currentUser : any = {
    img_url : 'assets/bg_imgs/1.jpg',
    name : 'Larix Win'
  };
  contactUser : any = {
    img_url : 'assets/bg_imgs/2.jpg',
    name : 'Livia Beatrix'
  };
  messages = [
    {
      side : 'send',
      msg : 'How are you?, I need your help for now. Are you avaible for hair style of me?',
      time : '12:00 AM'
    },
    {
      side : 'receive',
      msg : 'I am good?',
      time : '1:00 PM',
    },
    {
      side : 'send',
      msg : 'Great',
      time : '8:00 AM',
    },
    {
      side : 'send',
      msg : 'How are you?, I need your help for now. Are you avaible for hair style of me?',
      time : '12:00 AM'
    },
    {
      side : 'receive',
      msg : 'I am good?',
      time : '1:00 PM',
    },
    {
      side : 'send',
      msg : 'Great',
      time : '8:00 AM',
    },
    {
      side : 'send',
      msg : 'How are you?, I need your help for now. Are you avaible for hair style of me?',
      time : '12:00 AM'
    },
    {
      side : 'receive',
      msg : 'I am good?',
      time : '1:00 PM',
    },
    {
      side : 'send',
      msg : 'Great ',
      time : '8:00 AM',
    },
  ];
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage : Storage, public keyboard : Keyboard) {
    storage.get('type').then((val) => {
      this.type = val;
    });
  }
  toBottom() {
    setTimeout(() => {
      this.content.scrollToBottom(300);
    }, 1000);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ChattingPage');
    this.toBottom();
  }

  goBack(){
    console.log('Go Back');
    this.navCtrl.pop();
  }

  addMessage() {
    let nowTime = new Date();
    if(nowTime.toLocaleTimeString().length==12){
      this.messages.push({
        side : 'send',
        msg : this.message,
        time : nowTime.toLocaleTimeString().slice(0,5) + nowTime.toLocaleTimeString().slice(8,12),
      });
    } else {
      this.messages.push({
        side : 'send',
        msg : this.message,
        time : nowTime.toLocaleTimeString().slice(0,4) + nowTime.toLocaleTimeString().slice(7,12),
      });
    }

    this.message = '';
    this.toBottom();
  }


  eventHandler(keyCode: Number) {
    if(keyCode == 13) this.addMessage();
  }
}
