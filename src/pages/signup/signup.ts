import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions	 } from '@ionic-native/in-app-browser';
import {Storage} from "@ionic/storage";
/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  type : string = '';
  accepted : boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams,  private iab: InAppBrowser,
              private storage : Storage) {
    storage.get('type').then((val) => {
      this.type = val;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
    console.log(this.type);
  }
  launch(url : string){
    const options : InAppBrowserOptions = {
      zoom : 'no'
    };
    this.iab.create(url,'_blank',options);
  }

  insertPersonal() {
    this.navCtrl.push('PersonalPage');
  }
  gotoBack() {
    this.navCtrl.pop();
  }

}
