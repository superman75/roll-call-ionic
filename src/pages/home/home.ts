import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";

/**
 * Generated class for the HomePage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  type : string = 'client';
  findRoot = 'FindPage';
  profileRoot = 'ProfilePage';
  requestRoot = 'RequestPage';
  acceptedRoot = 'AcceptedPage';
  timingRoot = 'TimingPage';
  upcomingRoot = 'UpcomingPage';


  constructor(public navCtrl: NavController, public navParams : NavParams, private storage : Storage) {
    this.type = navParams.get('type');
    // storage.get('type').then((val) => {
    //   this.type = val;
    // });
  }
}
