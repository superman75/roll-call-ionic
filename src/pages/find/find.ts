import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireDatabase} from "angularfire2/database";
/**
 * Generated class for the FindPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-find',
  templateUrl: 'find.html',
})
export class FindPage {

  isShowCalendar : boolean = false;
  isShowStyles : boolean = false;
  isShowTime : boolean = false;

  stylist_list : any[];
  stylists : any = [];
  date : Date;
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  monthNames: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
  timeArray : string[] = [
    '12:00 AM - 2:00 PM',
    '2:00 PM - 4:00 PM',
    '4:00 PM - 6:00 PM',
    '6:00 PM - 8:00 PM'
  ];
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  objectSubscription : any;
  selectedDate : string = '';
  selectedStyle : string = '';
  selectedTime : string = '';
  selectedLocation : string = '';
  search : string = 'all';
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private afDB : AngularFireDatabase) {

    this.showCurrent();


  }

  ionViewWillEnter(){
    this.objectSubscription = this.afDB.object('/users').valueChanges()
      .subscribe(snapShots=>{
        let results = Object.keys(snapShots);

        this.stylist_list = [];
        this.stylists = [];
        results.forEach((key,index)=> {
          let user = snapShots[key];
          if(user.type == 'stylist') {
            this.stylist_list.push(user);
            this.stylists.push(user);
          }
        });
      })
  }
  ionViewWillLeave(){
    this.objectSubscription.unsubscribe();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FindPage');
  }

  getItems(ev : any){
    this.initializeItems();
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.stylists = this.stylists.filter((item) => {
        return (item.userNa.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  initializeItems() {
    this.stylists = this.stylist_list;

  }

  getDaysOfMonth() {
    this.daysInThisMonth = [];
    this.daysInLastMonth = [];
    this.daysInNextMonth = [];
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    if(this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
    } else {
      this.currentDate = 999;
    }

    let firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    let prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for(let i = prevNumOfDays-(firstDayThisMonth-1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i);
    }

    let thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDate();
    for (let i = 0; i < thisNumOfDays; i++) {
      this.daysInThisMonth.push(i+1);
    }

    let lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDay();
    for (let i = 0; i < (6-lastDayThisMonth); i++) {
      this.daysInNextMonth.push(i+1);
    }
    let totalDays = this.daysInLastMonth.length+this.daysInThisMonth.length+this.daysInNextMonth.length;
    if(totalDays<36) {
      for(let i = (7-lastDayThisMonth); i < ((7-lastDayThisMonth)+7); i++) {
        this.daysInNextMonth.push(i);
      }
    }

  }

  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.getDaysOfMonth();
  }

  showCurrent() {
    this.date = new Date();
    this.getDaysOfMonth();
  }

  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0);
    this.getDaysOfMonth();
  }


  toggleCalendar() {
    this.isShowCalendar = !this.isShowCalendar;
  }
  toggleStyles() {
    this.isShowStyles = !this.isShowStyles;
  }
  toggleTime() {
    this.isShowTime = !this.isShowTime;
  }
  selectDate(day) {
    this.selectedDate = this.monthNames[this.date.getMonth()] + " " + day + " " + this.date.getFullYear();
    this.toggleCalendar();
    this.initializeItems();
    // if the value is an empty string don't filter the items
    if (this.selectedDate && this.selectedDate.trim() != '') {
      this.stylists = this.stylists.filter((item) => {
        return (item.availableDate.toLowerCase().indexOf(this.selectedDate.toLowerCase()) > -1);
      })
    }
  }

  setStyle(num) {
    this.selectedStyle = 'Style #' + num;

    this.initializeItems();
    if (this.selectedStyle && this.selectedStyle.trim() != '') {
      this.stylists = this.stylists.filter((item) => {
        return (item.availableStyles.toLowerCase().indexOf(this.selectedStyle.toLowerCase()) > -1);
      })
    }
  }
  setTime(time) {
    this.selectedTime = time;

    this.initializeItems();
    if (this.selectedTime && this.selectedTime.trim() != '') {
      this.stylists = this.stylists.filter((item) => {
        return (item.availableTime.toLowerCase().indexOf(this.selectedTime.toLowerCase()) > -1);
      })
    }
  }

  setLocation() {
    this.initializeItems();
    if (this.selectedLocation && this.selectedLocation.trim() != '') {
      this.stylists = this.stylists.filter((item) => {
        return (item.location.toLowerCase().indexOf(this.selectedLocation.toLowerCase()) > -1);
      })
    }
  }

  openStylist (i) {

    this.closeAllItem();
    this.stylists[i].open = true;
  }
  closeStylist (i) {
    this.closeAllItem();
    this.stylists[i].open = false;
  }

  isExistStyle(i: number, style : string){
    return (this.stylists[i].availableStyles.toLowerCase().indexOf(style.toLowerCase()) > -1);
  }
  isExistTime(i: number, j : number){
    return (this.stylists[i].availableTime.toLowerCase().indexOf(this.timeArray[j].toLowerCase()) > -1);
  }
  closeAllItem(){
    for(let i = 0;i < this.stylist_list.length; i++) {
      this.stylist_list[i].open = false;
    }
  }

  pushToBooking(stylist: any) {
    this.navCtrl.push('BookingPage',{stylist : stylist});
  }
  openThread() {
    console.log('Open Chat');
    this.navCtrl.push('ThreadPage');
  }


}
