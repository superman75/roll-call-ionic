import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {AngularFireAuth} from "angularfire2/auth";
import {Appointment} from "../../providers/modals/modals";
import {AngularFireDatabase} from 'angularfire2/database';
import {ToastProvider} from "../../providers/toast/toast";

/**
 * Generated class for the BookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-booking',
  templateUrl: 'booking.html',
})
export class BookingPage {
  // profile = {
  //   img_url : 'assets/bg_imgs/redheads-women-dyed-hair.jpg',
  //   location : '1st Street 365 Washington, D.C',
  //   review : 4.6,
  //   name : 'Florence May',
  //   time : '2:00 PM - 4:00 PM, 4:00 PM - 6:00 PM',
  //   style : 'Style #1, Style #2, Style #3, Style #4',
  //   date : 'Oct 21 2018, Oct 23 2018, Oct 26 2018, Oct 18 2018, Oct 20 2018, Oct 22 2018',
  // };
  styleArray : string[] = [
    'Style #1',
    'Style #2',
    'Style #3',
    'Style #4',
  ];
  timeArray : string[] = [
    '12:00 AM - 2:00 PM',
    '2:00 PM - 4:00 PM',
    '4:00 PM - 6:00 PM',
    '6:00 PM - 8:00 PM'
  ];
  dateArray : string[];
  selectedDate :string = '';
  selectedStyle : string = '';
  selectedTime : string = '';
  stylist : any = {};

  newAppoint : Appointment = new Appointment();
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private storage : Storage,
              private afDB : AngularFireDatabase,
              private toastProvider: ToastProvider,
              private afAuth : AngularFireAuth) {
    this.dateArray = [];
    let d = new Date();
    for(let i = 0;i <7; i++){
      d.setDate(d.getDate() + 1);
      this.dateArray.push(d.toDateString().slice(4,15));
    }
    console.log(this.dateArray);
    this.stylist = this.navParams.get('stylist');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookingPage');
    this.newAppoint.stylistId = this.stylist.userId;
    this.afAuth.authState.take(1).subscribe( data=>{
      this.newAppoint.clientId = data.uid;
    });
  }
  ionViewWillEnter() {
    console.log('ionViewWillEnter BookingPage');
    this.storage.get('pop').then((val) => {
      if(val=='ok') this.navCtrl.pop();
      this.storage.set('pop','');
    });

  }
  gotoBack() {
    this.navCtrl.pop();
  }
  isExistTime(i : number){
    return (this.stylist.availableTime.toLowerCase().indexOf(this.timeArray[i].toLowerCase()) > -1);
  }
  isSelectedTime(i : number){
    return this.selectedTime == this.timeArray[i];
  }
  addTime(i : number) {
    if(this.selectedTime == this.timeArray[i]) this.selectedTime = '';
    else  this.selectedTime = this.timeArray[i];
  }
  isExistStyle(i : number){
    return (this.stylist.availableStyles.toLowerCase().indexOf(this.styleArray[i].toLowerCase()) > -1);
  }
  isSelectedStyle(i : number){
    return this.selectedStyle == this.styleArray[i];
  }
  addStyle(i : number) {
    if(this.selectedStyle == this.styleArray[i]) this.selectedStyle = '';
    else  this.selectedStyle = this.styleArray[i];
  }
  isExistDate(i : number){
    return (this.stylist.availableDate.toLowerCase().indexOf(this.dateArray[i].toLowerCase()) > -1);
  }
  isSelectedDate(i : number){
    return this.selectedDate == this.dateArray[i];
  }
  addDate(i : number) {
    if(this.selectedDate == this.dateArray[i]) this.selectedDate = '';
    else  this.selectedDate = this.dateArray[i];
  }

  booking(){
    console.log('booking!');
    this.newAppoint.availableDate = this.selectedDate;
    this.newAppoint.availableStyle = this.selectedStyle;
    this.newAppoint.availableTime = this.selectedTime;
    let objectSubscription = this.afDB.object('/appointments').valueChanges()
      .subscribe(snapShots=>{
        if(!snapShots){
          objectSubscription.unsubscribe();
          this.pushNewAppointment();
          return;
        }
        let results = Object.keys(snapShots);
        let count : number = 0;
        results.forEach((key,index)=> {
          console.log('testing');
          let appoint = snapShots[key];
          if(appoint.stylistId == this.newAppoint.stylistId && appoint.availableDate == this.newAppoint.availableDate && appoint.availableTime == this.newAppoint.availableTime) {
            count++;
            console.log(appoint);
            console.log(count);
          }
          if(count>3) {
            this.toastProvider.presentToast('There are already 3 appointments at same timeslot. Please book other at timeslot',1);
            objectSubscription.unsubscribe();
            return;
          }
          if(index==results.length-1){
            objectSubscription.unsubscribe();
            this.pushNewAppointment();
          }
        });
      });

  }

  availableBooking() {
    if(this.selectedTime == '') return false;
    if(this.selectedStyle == '') return false;
    if(this.selectedDate == '') return false;
    return true;
  }

  pushNewAppointment(){
    this.newAppoint.status = 0;
    this.newAppoint.createdDate = new Date().getTime();
    this.storage.set('newAppoint', this.newAppoint);
    let key = this.afDB.list('/appointments').push(this.newAppoint).key;
    console.log(key);

    this.newAppoint.appointmentId = key;
    this.afDB.object('/appointments/' + key).update(this.newAppoint);
    console.log(this.newAppoint);
    this.navCtrl.push('ConfirmationPage',{'request' : this.newAppoint, 'stylist': this.stylist});
  }

}
