import { Component } from '@angular/core';
import {ActionSheetController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {AngularFireAuth} from "angularfire2/auth";
import {AngularFireDatabase} from "angularfire2/database";
import {Storage} from "@ionic/storage";
import {Camera} from "@ionic-native/camera";
import firebase from 'firebase';
import {EditProfilePage} from "../edit-profile/edit-profile";
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  type : string = 'client';
  currentUser :   any = {} ;
  private picdata : any;
  private mypicref : any;
  objectSubscription : any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private afAuth : AngularFireAuth,
              private storage : Storage,
              private loadingCtrl : LoadingController,
              private afDB : AngularFireDatabase,
              public actionSheetCtrl : ActionSheetController,
              private camera : Camera) {


  }

  ionViewWillEnter() {
    console.log('ionViewWillLoad ProfilePage');
    this.storage.get('type').then((val) => {
      this.type = val;
      this.afAuth.authState.take(1).subscribe( data=>{
        this.objectSubscription = this.afDB.object('/users/'+data.uid).valueChanges()
          .subscribe(user=>{
            this.currentUser = user;
            console.log(JSON.stringify(this.currentUser));
          })
      });
    });
    this.mypicref = firebase.storage().ref('/pictures/');
  }

  ionViewWillLeave(){
    this.objectSubscription.unsubscribe();
  }

  isExistStyle(style : string){
    return (this.currentUser.availableStyles.toLowerCase().indexOf(style.toLowerCase()) > -1);
  }


  pushToEditPage() {
    this.navCtrl.push('EditProfilePage');
  }

}
