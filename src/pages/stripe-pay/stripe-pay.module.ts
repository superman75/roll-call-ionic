import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StripePayPage } from './stripe-pay';

@NgModule({
  declarations: [
    StripePayPage,
  ],
  imports: [
    IonicPageModule.forChild(StripePayPage),
  ],
  exports : [
    StripePayPage
  ]
})
export class StripePayPageModule {}
