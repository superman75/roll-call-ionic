import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {API_KEY} from "../../util/constants";

/**
 * Generated class for the StripePayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare let Stripe;

@IonicPage()
@Component({
  selector: 'page-stripe-pay',
  templateUrl: 'stripe-pay.html',
})
export class StripePayPage {

  stripe = Stripe(API_KEY);
  card : any;
  form : any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StripePayPage');

    this.setupStripe();
  }
  setupStripe(){
    let elements = this.stripe.elements({fonts: [
      {
        cssSrc: 'https://fonts.googleapis.com/css?family=Source+Code+Pro',
      },
    ],});

    let elementStyles = {
      base: {
        color: "#32325D",
        fontWeight: 500,
        fontFamily: "Inter UI, Open Sans, Segoe UI, sans-serif",
        fontSize: "16px",
        fontSmoothing: "antialiased",

        "::placeholder": {
          color: "#CFD7DF"
        }
      },
      invalid: {
        color: "#E25950"
      }
    };

    this.card = elements.create("card",{style: elementStyles});
    this.card.mount("#example4-card");


    // let paymentRequest = this.stripe.paymentRequest({
    //   country: "US",
    //   currency: "usd",
    //   total: {
    //     amount: 2000,
    //     label: "Total"
    //   }
    // });
    // paymentRequest.on("token", function(result) {
    //   var example = document.querySelector(".example4");
    //   example.querySelector(".token").innerText = result.token.id;
    //   example.classList.add("submitted");
    //   result.complete("success");
    // });
    //
    // var paymentRequestElement = elements.create("paymentRequestButton", {
    //   paymentRequest: paymentRequest,
    //   style: {
    //     paymentRequestButton: {
    //       type: "donate"
    //     }
    //   }
    // });
    //
    // paymentRequest.canMakePayment().then(function(result) {
    //   if (result) {
    //     document.querySelector(".example4 .card-only").style.display = "none";
    //     document.querySelector(
    //       ".example4 .payment-request-available"
    //     ).style.display =
    //       "block";
    //     paymentRequestElement.mount("#example4-paymentRequest");
    //   }
    // });

    this.registerElements([this.card], 'example4');

  }
  enableInputs() {
    let self = this;
    Array.prototype.forEach.call(
      self.form.querySelectorAll(
        "input[type='text'], input[type='email'], input[type='tel']"
      ),
      function(input) {
        input.removeAttribute('disabled');
      }
    );
  }

  disableInputs() {
    let self = this;
    Array.prototype.forEach.call(
      self.form.querySelectorAll(
        "input[type='text'], input[type='email'], input[type='tel']"
      ),
      function(input) {
        input.setAttribute('disabled', 'true');
      }
    );
  }

  triggerBrowserValidation() {
    // The only way to trigger HTML5 form validation UI is to fake a user submit
    // event.
    let submit = document.createElement('input');
    submit.type = 'submit';
    submit.style.display = 'none';
    this.form.appendChild(submit);
    submit.click();
    submit.remove();
  }

  registerElements(elements, exampleName) {
    let formClass = '.' + exampleName;
    let example = document.querySelector(formClass);

    this.form = example.querySelector('form');
    // let resetButton = example.querySelector('a.reset');
    let error = this.form.querySelector('.error');
    let errorMessage = error.querySelector('.message');

    let self = this;
    let savedErrors = {};
    elements.forEach(function(element, idx) {
      element.on('change', function(event) {
        if (event.error) {
          error.classList.add('visible');
          savedErrors[idx] = event.error.message;
          errorMessage.innerText = event.error.message;
        } else {
          savedErrors[idx] = null;

          // Loop over the saved errors and find the first one, if any.
          let nextError = Object.keys(savedErrors)
            .sort()
            .reduce(function(maybeFoundError, key) {
              return maybeFoundError || savedErrors[key];
            }, null);

          if (nextError) {
            // Now that they've fixed the current error, show another one.
            errorMessage.innerText = nextError;
          } else {
            // The user fixed the last error; no more errors.

            error.classList.remove('visible');
            errorMessage.innerText = '';
          }
        }
      });
    });

    // Listen on the form's 'submit' handler...

    self.form.addEventListener('submit', function(e) {
      e.preventDefault();
      console.log('submit button clicked');
      // Trigger HTML5 validation UI on the form if any of the inputs fail
      // validation.
      let plainInputsValid = true;
      Array.prototype.forEach.call(self.form.querySelectorAll('input'), function(
        input
      ) {
        if (input.checkValidity && !input.checkValidity()) {
          plainInputsValid = false;
          console.log(plainInputsValid);
          return;
        }
      });
      if (!plainInputsValid) {
        self.triggerBrowserValidation();
        return;
      }

      // Show a loading screen...
      example.classList.add('submitting');

      // Disable all inputs.
      self.disableInputs();


      // Use Stripe.js to create a token. We only need to pass in one Element
      // from the Element group in order to create a token. We can also pass
      // in the additional customer data we collected in our form.
      self.stripe.createToken(elements[0]).then(function(result) {
        // Stop loading!
        example.classList.remove('submitting');

        if (result.token) {
          console.log(result.token.id);
          // If we received a token, show the token ID.
          // example.querySelector('.token').innerText = result.token.id;
          // example.classList.add('submitted');
        } else {
          // Otherwise, un-disable inputs.
          self.enableInputs();
        }
      });
    });

  }
}
