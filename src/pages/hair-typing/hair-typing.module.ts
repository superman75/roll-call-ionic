import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HairTypingPage } from './hair-typing';

@NgModule({
  declarations : [
    HairTypingPage
  ],
  imports: [
    IonicPageModule.forChild(HairTypingPage),
  ],
  exports : [
    HairTypingPage
  ]
})
export class HairTypingPageModule {}
