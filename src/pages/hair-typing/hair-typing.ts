import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {User} from "../../providers/modals/modals";
import {AngularFireDatabase} from "angularfire2/database";
import { AngularFireAuth } from 'angularfire2/auth';
import {ToastProvider} from "../../providers/toast/toast";

import * as firebase from 'firebase';

declare let google;
/**
 * Generated class for the HairTypingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hair-typing',
  templateUrl: 'hair-typing.html',
})
export class HairTypingPage {
  type : string =  '';
  typing_list : any[] = [
    {
      number : 1,
      open : false,
      desc : '',
      content :   [
        {
          name : '1A',
          desc : 'Type 1A hair is described as fine, very thin and soft with a noticeable shine.',
          open : false
        },
        {
          name : '1B',
          desc : 'Type 1B hair is medium-textured and has more body than Type 1A hair.',
          open : false
        },
        {
          name : '1C',
          desc : 'Type 1C hair is the most resistant to curly styling and relatively coarse compared to other Type 1 hair types.',
          open : false
        }
      ]
    },
    {
      number : 2,
      open : false,
      desc : '',
      content :   [
        {
          name : '2A',
          desc : 'Type 2A hair is fine and thin. It is relatively easy to handle from a styling perspective because it can easily be straightened or curled.',
          open : false
        },
        {
          name : '2B',
          desc : 'Type 2B hair characteristically has waves that tend to adhere to the shape of your head.',
          open : false
        },
        {
          name : '2C',
          desc : 'Type 2C hair will frizz easily and it is fairly coarse.',
          open : false
        }
      ]
    },
    {
      number : 3,
      open : false,
      desc : '',
      content :   [
        {
          name : '3A',
          desc : 'Type 3A hair is very shinny and loose.',
          open : false
        },
        {
          name : '3B',
          desc : 'Type 3B hair has a medium amount curls, ranging from bouncy ringlets (spiral like curls of hair) to tight corkscrews (spiral-shaped corkscrew curls).',
          open : false
        },
        {
          name : '3C',
          desc : 'Type 3C hair isn’t a part of the Andre Walker Hair Typing System. Please see the “what’s missing” section below for more information',
          open : false
        }
      ]
    },
    {
      number : 4,
      open : false,
      desc : '',
      content :   [
        {
          name : '4A',
          desc : 'Type 4A hair is full of tight coils. It has a “S” pattern when stretched, much like Type 3 curly hair.',
          open : false
        },
        {
          name : '4B',
          desc : 'Type 4B hair has a less defined pattern of curls and looks more like a “Z” as the hair bends with very sharp angles.',
          open : false
        },
        {
          name : '4C',
          desc : 'Type 4C hair isn’t a part of the Andre Walker Hair Typing System. Please see the “what’s missing” section below for more information',
          open : false
        }
      ]
    }
  ];
  regUser: User = new User();
  password : string;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storage : Storage,
              private afDB : AngularFireDatabase,
              private loadingCtrl : LoadingController,
              private toastProvider : ToastProvider,
              private afAuth : AngularFireAuth
              ) {
    storage.get('type').then((val) => {
      this.type = val;
    });
    storage.get('password').then((val) => {
      this.password = val;
      console.log(this.password);
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HairTypingPage');
    this.storage.get('regUser').then((val)=>{
      this.regUser = val;
      this.regUser.hairTyping = '';
      console.log(this.regUser.email);
    });
  }

  gotoBack() {
    this.navCtrl.pop();
  }



  pushToLogin() {
    this.getAvailableStyles();
    let loading = this.loadingCtrl.create({
      content: 'Registering user ..',
      dismissOnPageChange: true
    });
    loading.present();
    this.afAuth.auth.createUserWithEmailAndPassword(this.regUser.email, this.password)
      .then((res)=>{

      console.log(res);
      loading.dismiss();
      let user = firebase.auth().currentUser;
      this.regUser.userId = user.uid;
      user.sendEmailVerification()
        .then(res => {
          console.log(res);
          this.toastProvider.presentToast("Email Verification is sent",0);
          this.afDB.object('/users/' + user.uid).set(this.regUser);
        })
        .catch(err =>{
          console.log(err);
          this.toastProvider.presentToast(err.message,2);
        });
        this.navCtrl.popToRoot();
      },(err)=>{
        loading.dismiss();
        this.toastProvider.presentToast(err.message,2);

      });
  }


  updateHairType(hairType : string) {
    if(!this.isExist(hairType)){
      this.regUser.hairTyping += hairType;
    } else {
      this.regUser.hairTyping = this.regUser.hairTyping.replace(hairType,'');
    }
    console.log(this.regUser.hairTyping);
  }

  getAvailableStyles(){
    this.regUser.availableStyles = '';
    for(let i = 1 ;i <=4 ; i++ ){
      if(this.regUser.hairTyping.indexOf('' + i)>-1){
        this.regUser.availableStyles += ' Style #' + i;
      }
    }
  }
  isExist(hairType : string){
    if(!this.regUser.hairTyping)return false;
    return this.regUser.hairTyping.toLowerCase().indexOf(hairType.toLowerCase())>-1;
    // return false;
  }

  openDetail(items, item) {
    items.content.forEach(value=>value.open=false);
    item.open = true;
    items.open = true;
    items.desc = item.desc;
  }
  closeDetail(items, item) {
    items.open = false;
    item.open = false;
  }
}
