import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {User} from "../../providers/modals/modals";

/**
 * Generated class for the PorosityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-porosity',
  templateUrl: 'porosity.html',
})
export class PorosityPage {

  type : string = 'client';
  regUser : User = new User();
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage : Storage) {
    storage.get('type').then((val) => {
      this.type = val;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PorosityPage');
    this.storage.get('regUser').then((val)=>{
      this.regUser = val;
    })
  }

  gotoBack() {
    this.navCtrl.pop();
  }
  pushToHairType() {
    this.storage.set('regUser', this.regUser);
    this.navCtrl.push('HairTypingPage');
  }

  updatePorosity(porosity) {
    this.regUser.porosity = porosity;
  }
}
