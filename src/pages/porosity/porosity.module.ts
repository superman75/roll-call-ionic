import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PorosityPage } from './porosity';

@NgModule({
  declarations: [
    PorosityPage
  ],
  imports: [
    IonicPageModule.forChild(PorosityPage),
  ],
  exports :[
    PorosityPage
  ]
})
export class PorosityPageModule {}
