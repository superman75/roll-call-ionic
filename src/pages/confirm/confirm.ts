import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {Storage} from "@ionic/storage";

/**
 * Generated class for the ConfirmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm',
  templateUrl: 'confirm.html',
})
export class ConfirmPage {

  type : string = '';
  constructor(public viewCtrl : ViewController, public params: NavParams, private storage: Storage) {
    storage.get('type').then((val) => {
      this.type = val;
    });
  }

  dismiss() {
    let data={action : 'dismiss'};
    this.viewCtrl.dismiss(data);
  }
}
