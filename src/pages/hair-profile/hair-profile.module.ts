import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HairProfilePage } from './hair-profile';

@NgModule({
  declarations: [
    HairProfilePage
  ],
  imports: [
    IonicPageModule.forChild(HairProfilePage),
  ],
  exports : [
    HairProfilePage
  ]
})
export class HairProfilePageModule {}
