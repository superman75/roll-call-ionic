import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {User} from "../../providers/modals/modals";

/**
 * Generated class for the HairProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hair-profile',
  templateUrl: 'hair-profile.html',
})
export class HairProfilePage {

  type : string = 'client';
  regUser : User = new User();
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage : Storage) {
    this.storage.get('regUser').then(value=>{
      this.regUser = value;
      console.log(this.regUser);
    });
    this.storage.get('type').then((val) => {
      this.type = val;
    });
  }
  ionViewWillLoad() {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HairProfilePage');
  }

  gotoBack() {
    this.navCtrl.pop();
  }
  pushToPorosity() {
    this.storage.set('regUser',this.regUser);
    this.navCtrl.push('PorosityPage');
  }

  updateHairProfile(hairProfile){

    this.regUser.hairProfile = hairProfile;
  }
}
