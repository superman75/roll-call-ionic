import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {AngularFireDatabase} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";
import {ToastProvider} from "../../providers/toast/toast";

/**
 * Generated class for the AcceptedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-accepted',
  templateUrl: 'accepted.html',
})
export class AcceptedPage {


  isShowCalendar : boolean = false;
  isShowTime : boolean = false;
  date : Date;
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  monthNames: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  timeArray : string[] = [
    '12:00 AM - 2:00 PM',
    '2:00 PM - 4:00 PM',
    '4:00 PM - 6:00 PM',
    '6:00 PM - 8:00 PM'
  ];
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  selectedDate : string = '';
  selectedTime : string = '';
  search : string = 'today';
  objectSubscription : any;
  objectSubscription1 : any;
  currentUserId : string;
  client_list :any = [];
  clients : any = [];
  reservation_list : any = [];
  reservations : any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private storage : Storage,
              private afAuth : AngularFireAuth,
              private toastProvider : ToastProvider,
              private alertCtrl : AlertController,
              private afDB : AngularFireDatabase) {

    this.afAuth.authState.take(1).subscribe( data=>{
      this.currentUserId = data.uid;
    });
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad AcceptedPage');

    this.initializeItems();
    this.showCurrent();
  }
  ionViewWillEnter() {
    this.objectSubscription = this.afDB.object('/appointments').valueChanges()
      .subscribe(snapShots=>{
        let results = Object.keys(snapShots);
        this.reservation_list = [];
        this.reservations = [];
        results.forEach((key,index)=> {
          let appointment = snapShots[key];
          if((appointment.status>0 && appointment.status<4)  && appointment.stylistId == this.currentUserId) {
            this.reservation_list.push(appointment);
            this.reservations.push(appointment);
          }
        });
      });
    this.objectSubscription1 = this.afDB.object('/users').valueChanges()
      .subscribe(snapShots=>{
        let results = Object.keys(snapShots);
        this.client_list = [];
        this.clients = [];
        results.forEach((key,index)=> {
          let user = snapShots[key];
          if(user.type == 'client') {
            this.clients.push(user);
            this.client_list.push(user);
          }
        });
      })
  }

  ionViewWillLeave() {
    this.objectSubscription.unsubscribe();
    this.objectSubscription1.unsubscribe();
  }


  getItems(ev : any){
    this.initializeItems();
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.clients = this.clients.filter((item) => {
        return (item.userName.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  initializeItems() {
    this.reservations = this.reservation_list;
    console.log(this.reservations);
    this.clients = this.client_list;
    console.log(this.clients);
  }

  getDaysOfMonth() {
    this.daysInThisMonth = [];
    this.daysInLastMonth = [];
    this.daysInNextMonth = [];
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    if(this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
    } else {
      this.currentDate = 999;
    }

    let firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    let prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for(let i = prevNumOfDays-(firstDayThisMonth-1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i);
    }

    let thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDate();
    for (let i = 0; i < thisNumOfDays; i++) {
      this.daysInThisMonth.push(i+1);
    }

    let lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDay();
    for (let i = 0; i < (6-lastDayThisMonth); i++) {
      this.daysInNextMonth.push(i+1);
    }
    let totalDays = this.daysInLastMonth.length+this.daysInThisMonth.length+this.daysInNextMonth.length;
    if(totalDays<36) {
      for(let i = (7-lastDayThisMonth); i < ((7-lastDayThisMonth)+7); i++) {
        this.daysInNextMonth.push(i);
      }
    }
  }

  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.getDaysOfMonth();
  }

  showCurrent() {
    this.date = new Date();
    this.getDaysOfMonth();
  }

  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0);
    this.getDaysOfMonth();
  }


  toggleCalendar() {
    this.isShowCalendar = !this.isShowCalendar;
  }

  toggleTime() {
    this.isShowTime = !this.isShowTime;
  }
  selectDate(day) {
    this.selectedDate = this.monthNames[this.date.getMonth()] + " " + day + " " + this.date.getFullYear();
    this.toggleCalendar();
    this.initializeItems();

    // if the value is an empty string don't filter the items
    if (this.selectedDate && this.selectedDate.trim() != '') {
      this.reservations = this.reservations.filter((item) => {
        return (item.availableDate.toLowerCase().indexOf(this.selectedDate.toLowerCase()) > -1);
      })
    }
  }

  setTime(time) {
    this.selectedTime = time;

    this.initializeItems();
    if (this.selectedTime && this.selectedTime.trim() != '') {
      this.reservations = this.reservations.filter((item) => {
        return (item.availableTime.toLowerCase().indexOf(this.selectedTime.toLowerCase()) > -1);
      })
    }
  }


  isExistTime(i: number, j : number){
    return (this.reservations[i].availableTime.toLowerCase().indexOf(this.timeArray[j].toLowerCase()) > -1);
  }

  openThread() {
    console.log('Open Chat');
    this.navCtrl.push('ThreadPage');
  }



  searchForToday() {
    let today = new Date().toDateString().slice(4,15);
    console.log(today);
    this.initializeItems();
    this.reservations = this.reservations.filter(reservation=>{
      return reservation.availableDate==today;
    });

  }

  searchForTime(){
    this.toggleTime();
    this.initializeItems();
  }

  doneRequest(reservation, client) {
    let alert = this.alertCtrl.create({
      title: 'Complete',
      message: 'Are you sure to complete ' + client.userName + "'s appointment?",
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sure',
          handler: () => {
            console.log('Sure clicked');
            reservation.status = 10;
            this.afDB.object('/appointments/' + reservation.appointmentId).update(reservation);
            // this.navCtrl.push(ConfirmationPage, {'request' : request, 'client' : client});
          }
        }
      ]
    });
    alert.present();


  }
  cancelRequest(reservation, client) {
    let alert = this.alertCtrl.create({
      title: 'Cancel',
      message: 'Do you want to cancel ' + client.userName + "'s appoinment?",
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('No clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {

            reservation.status = 4;
            this.afDB.object('/appointments/' + reservation.appointmentId).update(reservation);
            this.toastProvider.presentToast('You declined ' + client.userName + "'s request.",1)
          }
        }

      ]
    });
    alert.present();
  }
}
