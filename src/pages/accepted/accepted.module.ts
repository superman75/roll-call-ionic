import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcceptedPage } from './accepted';

@NgModule({
  declarations: [
    AcceptedPage,
  ],
  imports: [
    IonicPageModule.forChild(AcceptedPage),
  ],
  exports : [
    AcceptedPage
  ]
})
export class AcceptedPageModule {}
