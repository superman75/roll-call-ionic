import {Component, NgZone} from '@angular/core';
import {ActionSheetController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../providers/modals/modals";
import {ToastProvider} from "../../providers/toast/toast";
import firebase from 'firebase';
import {Camera} from "@ionic-native/camera";
import {Geolocation} from "@ionic-native/geolocation";

declare let google;
/**
 * Generated class for the PersonalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-personal',
  templateUrl: 'personal.html',
})
export class PersonalPage {

  type : string =  '';
  autocompleteItems = [];
  registerForm : FormGroup;
  regUser : User = new User();
  repeatPassword : string;
  password : string;
  private picdata : any;
  private mypicref : any;

  service = new google.maps.places.AutocompleteService();

  timeArray : string[] = [
    '12:00 AM - 2:00 PM',
    '2:00 PM - 4:00 PM',
    '4:00 PM - 6:00 PM',
    '6:00 PM - 8:00 PM'
  ];
  dateArray : string[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storage : Storage,
              private fb : FormBuilder,
              private loadingCtrl : LoadingController,
              private toastProvider : ToastProvider,
              private geolocation : Geolocation,
              private zone: NgZone,
              public actionSheetCtrl : ActionSheetController,
              private camera : Camera) {``
    storage.get('type').then((val) => {
      this.type = val;
      this.regUser.type = this.type;
    });

    this.regUser.profileImg = 'assets/bg_imgs/blank_image.png';
    this.regUser.userName = 'Nick Bey';
    this.regUser.salonName = 'Beauty';
    this.regUser.email = 'superman19900221@gmail.com';
    this.password = '123456';
    this.repeatPassword = '123456';
    this.regUser.qa1 = 'Qq';
    this.regUser.qa2 = 'Aa';
    this.regUser.qa3 = 'Zz';
    this.registerForm = this.buildRegisterForm();
    this.regUser.open  = false;
    this.regUser.reviewCount = 0;
    this.regUser.reviewNum = 0;
    this.regUser.availableTime = '';
    this.regUser.availableDate = '';
    this.dateArray = [];
    let d = new Date();
    for(let i = 0;i < 7; i++){
      d.setDate(d.getDate() + 1);
      this.dateArray.push(d.toDateString().slice(4,15));
    }

    let self = this;
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log('lat : ' + resp.coords.latitude + ' , long : ' +  resp.coords.longitude);
      let latlng = {lat : resp.coords.latitude, lng : resp.coords.longitude};
      let geoCoder = new google.maps.Geocoder();
      geoCoder.geocode({'location' : latlng}, function(results, status){
        if(status=='OK'){
          if(results[0]){
            self.regUser.location = results[0].formatted_address;
            console.log(this.regUser.location);
            if(!this.regUser.location) this.regUser.location = 'unknown';
            // console.log(results[0].formatted_address);
          } else
            self.toastProvider.presentToast('No results found.',1);
        } else {
          self.toastProvider.presentToast('Geocoder failed due to ' + status,1);
        }
      })
    }).catch((error) => {
      console.log('Error getting location', error);
    });


  }

  ionViewDidLoad() {
    this.mypicref = firebase.storage().ref('/pictures/');
    console.log('ionViewDidLoad PersonalPage');
  }

  gotoBack() {
    this.navCtrl.pop();
  }
  pushToHairProfile() {
    if(this.registerForm.invalid){
      this.toastProvider.presentToast("Please insert valid email and password should be of minimum 6 characteres.",1);
      return;
    }

    if(this.password != this.repeatPassword){
      this.toastProvider.presentToast("Password and Confirm Password is not matched!",1);
      return;
    }

    if(!this.regUser.userName || !this.regUser.location || !this.regUser.qa1 || !this.regUser.qa2 || !this.regUser.qa3){
      this.toastProvider.presentToast("Please fill in all required fields.",1);
      return;
    }
    if(this.type=='stylist'){
      if(!this.regUser.availableTime || !this.regUser.availableDate){
        this.toastProvider.presentToast("Please select available date&times.",1);
        return;
      }
    }
    if(this.regUser.profileImg == 'assets/bg_imgs/blank_image.png'){
      this.toastProvider.presentToast("Please upload your photo",1);
      return;
    }
    this.storage.set('password', this.password);
    this.storage.set('regUser', this.regUser);
    this.navCtrl.push('HairProfilePage');
  }

  buildRegisterForm() {
    return this.fb.group({
      email: ['', [Validators.required, Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/)]],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6),Validators.maxLength(100)])]  ,
      username : [''],
      repeatpassword : [''],
      location : [''],
      qa1 : [''],
      qa2 : [''],
      qa3 : ['']
    })
  }

  takeNewPic(from : number){
    console.log("Take new photo");
    this.camera.getPicture({
      quality : 100,
      destinationType : this.camera.DestinationType.DATA_URL,
      sourceType : from,
      encodingType : this.camera.EncodingType.PNG,
      allowEdit : true,
      targetWidth : 250,
      targetHeight : 250,
      saveToPhotoAlbum : false
    }).then(imagedata =>{
      this.picdata = imagedata;
      this.upload();
    })
  }

  upload(){

    let loading = this.loadingCtrl.create({
      content: 'uploading...'
    });

    loading.present();
    this.mypicref.child(this.uid()).child('pic.png')
      .putString(this.picdata,'base64',{contentType : 'image/png'})
      .then(savepic =>{        // this.regUser.profileImg = savepic.downloadURL;
        // console.log(savepic.downloadURL);
        savepic.ref.getDownloadURL().then((url)=>{
          this.regUser.profileImg = url;
          loading.dismiss();
          console.log(url);
        })
      })
  }

  uid() {
    let d = new Date().getTime();
    return 'xxxxxxxx-xxxx-4xxx-yxxx'.replace(/[xy]/g, function (c) {
      let r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }


  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Take your photo',
      buttons: [
        {
          text: 'From Camera',
          handler: () => {
            this.takeNewPic(1);
          }
        },
        {
          text: 'From Gallery',
          handler: () => {
            this.takeNewPic(0);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });

    actionSheet.present();
  }


  updateSearch() {

    if (this.regUser.location == '') {
      this.autocompleteItems = [];
      return;
    }

    let me = this;
    this.service.getPlacePredictions({
      input: this.regUser.location,
      // componentRestrictions: {
      //   country: 'de'
      // }
    }, (predictions, status) => {
      me.autocompleteItems = [];

      me.zone.run(() => {
        if (predictions != null) {
          predictions.forEach((prediction) => {
            me.autocompleteItems.push(prediction.description);
          });
        }
      });
    });
  }
  isSelectedTime(i : number) {
    return this.regUser.availableTime.toLowerCase().indexOf(this.timeArray[i].toLowerCase())>-1;
  }

  addTime(i : number){
    console.log(this.regUser.availableTime);
    if(this.isSelectedTime(i)) this.regUser.availableTime = this.regUser.availableTime.replace(this.timeArray[i],'');
    else this.regUser.availableTime += this.timeArray[i];
  }

  isSelectedDate(i : number) {
    return (this.regUser.availableDate.toLowerCase().indexOf(this.dateArray[i].toLowerCase()) > -1);
  }

  addDate(i : number) {
    if(this.isSelectedDate(i)) this.regUser.availableDate = this.regUser.availableDate.replace(this.dateArray[i],'');
    else  this.regUser.availableDate += this.dateArray[i];
  }
}
