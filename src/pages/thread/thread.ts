import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Storage} from "@ionic/storage";

/**
 * Generated class for the ThreadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-thread',
  templateUrl: 'thread.html',
})
export class ThreadPage {

  chats = [
    {
    img_url : 'assets/bg_imgs/1.jpg',
    name : 'Florence May',
    lastMsg : 'Hi.  How are you?',
    lastTime : '12:00 PM'
    },
    {
      img_url : 'assets/bg_imgs/2.jpg',
      name : 'Wingston Walker',
      lastMsg : 'That sounds good',
      lastTime : '12:45 PM'
    },
    {
      img_url : 'assets/bg_imgs/3.jpg',
      name : 'Maria Wager',
      lastMsg : 'Who can help me now?',
      lastTime : '2:00 PM'
    },
  ];
  type : string =  'client';
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
    storage.get('type').then((val) => {
      this.type = val;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ThreadPage');
  }
  goBack(){
    this.navCtrl.pop();
  }
  openChat() {
    this.navCtrl.push('ChattingPage');
  }
}
