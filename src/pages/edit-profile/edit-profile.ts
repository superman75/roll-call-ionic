import { Component } from '@angular/core';
import {ActionSheetController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {AngularFireAuth} from "angularfire2/auth";
import {AngularFireDatabase} from "angularfire2/database";
import {Storage} from "@ionic/storage";
import {Camera} from "@ionic-native/camera";
import firebase from 'firebase';
/**
 * Generated class for the EditProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

  type : string = 'client';
  currentUser :   any = {} ;
  picdata : any;
  mypicref : any;
  objectSubscription : any;

  timeArray : string[] = [
    '12:00 AM - 2:00 PM',
    '2:00 PM - 4:00 PM',
    '4:00 PM - 6:00 PM',
    '6:00 PM - 8:00 PM'
  ];
  dateArray : string[];


  constructor(public navCtrl: NavController, public navParams: NavParams,
              private afAuth : AngularFireAuth,
              private storage : Storage,
              private loadingCtrl : LoadingController,
              private afDB : AngularFireDatabase,
              public actionSheetCtrl : ActionSheetController,
              private camera : Camera) {
    this.storage.get('type').then((val)=>{
      this.type = val;
    });

    this.dateArray = [];
    let d = new Date();
    for(let i = 0;i < 7; i++){
      d.setDate(d.getDate() + 1);
      this.dateArray.push(d.toDateString().slice(4,15));
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProfilePage');
  }


  ionViewWillEnter() {
    console.log('ionViewWillLoad ProfilePage');
    this.afAuth.authState.take(1).subscribe( data=>{
      this.objectSubscription = this.afDB.object('/users/'+data.uid).valueChanges()
        .subscribe(user=>{
          this.currentUser = user;
          console.log(JSON.stringify(this.currentUser));
        })
    });
    this.mypicref = firebase.storage().ref('/pictures/');
  }

  ionViewWillLeave(){
    this.objectSubscription.unsubscribe();
  }

  takeNewPic(from : number){
    console.log("Take new photo");
    this.camera.getPicture({
      quality : 100,
      destinationType : this.camera.DestinationType.DATA_URL,
      sourceType : from,
      encodingType : this.camera.EncodingType.PNG,
      allowEdit : true,
      targetWidth : 250,
      targetHeight : 250,
      saveToPhotoAlbum : false
    }).then(imagedata =>{


      this.picdata = imagedata;
      this.upload();
    }).catch(error=>{
      console.log(error);
    })
  }


  upload(){
    let loading = this.loadingCtrl.create({
      content: 'uploading...'
    });

    loading.present();
    this.mypicref.child(this.uid()).child('pic.png')
      .putString(this.picdata,'base64',{contentType : 'image/png'})
      .then(savepic =>{        // this.regUser.profileImg = savepic.downloadURL;
        // console.log(savepic.downloadURL);
        savepic.ref.getDownloadURL().then((url)=>{
          this.afDB.object('/users/' + this.currentUser.userId + '/profileImg').set(url);
          console.log(url);
          loading.dismiss();
        })
      })
  }

  uid() {
    let d = new Date().getTime();
    return 'xxxxxxxx-xxxx-4xxx-yxxx'.replace(/[xy]/g, function (c) {
      let r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }



  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Take your photo',
      buttons: [
        {
          text: 'From Camera',
          handler: () => {
            this.takeNewPic(1);
          }
        },
        {
          text: 'From Gallery',
          handler: () => {
            this.takeNewPic(0);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });

    actionSheet.present();
  }

  gotoBack(){
    this.navCtrl.pop();
  }

  updateUser(){
    this.afDB.object('/users/' + this.currentUser.userId).update(this.currentUser);
    this.gotoBack();
  }

  isSelectedTime(i : number) {
    return this.currentUser.availableTime.toLowerCase().indexOf(this.timeArray[i].toLowerCase())>-1;
  }

  addTime(i : number){
    console.log(this.currentUser.availableTime);
    if(this.isSelectedTime(i)) this.currentUser.availableTime = this.currentUser.availableTime.replace(this.timeArray[i],'');
    else this.currentUser.availableTime += this.timeArray[i];
  }

  isSelectedDate(i : number) {
    return (this.currentUser.availableDate.toLowerCase().indexOf(this.dateArray[i].toLowerCase()) > -1);
  }

  addDate(i : number) {
    if(this.isSelectedDate(i)) this.currentUser.availableDate = this.currentUser.availableDate.replace(this.dateArray[i],'');
    else  this.currentUser.availableDate += this.dateArray[i];
  }
}
