import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {ConfirmationPage} from "../confirmation/confirmation";
import {AngularFireDatabase} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";
import {ToastProvider} from "../../providers/toast/toast";
/**
 * Generated class for the RequestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-request',
  templateUrl: 'request.html',
})
export class RequestPage {
  reservation_list = [
    {
      img_url : 'assets/bg_imgs/1.jpg',
      name : 'Florence May',
      location : '1st street 365 Washington. DC',
      date : 'October 9, 2018',
      time : '12:00 AM - 2:00 PM',
      style : 'Style #1',
      status : 0,
      open : false
    },
    {
      img_url : 'assets/bg_imgs/2.jpg',
      name : 'Emma Johnson',
      location : 'Columbia Road 748 Manhattan',
      date : 'September 9, 2018',
      time : '12:00 AM - 2:00 PM',
      review : 4,
      style : 'Style #2',
      status : 1,
      open : false
    },
    {
      img_url : 'assets/bg_imgs/3.jpg',
      name : 'Sharran Hernandez',
      location : 'Banneker Circle 158 New York',
      date : 'September 28, 2018',
      time : '2:00 PM - 4:00 PM',
      style : 'Style #3',
      status : 2,
      open : false
    },
    {
      img_url : 'assets/bg_imgs/4.jpg',
      name : 'Amrew Were',
      location : 'Banneker Circle 158 New York',
      date : 'September 28, 2018',
      time : '2:00 PM - 4:00 PM',
      style : 'Style #3',
      status : 2,
      open : false
    },
  ];
  clients : any = [];
  objectSubscription : any;
  objectSubscription1 : any;
  currentUserId : string;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private storage :  Storage,
              private afAuth : AngularFireAuth,
              private toastProvider : ToastProvider,
              private alertCtrl : AlertController,
              private afDB : AngularFireDatabase) {
    this.afAuth.authState.take(1).subscribe( data=>{
      this.currentUserId = data.uid;
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RequestPage');
  }
  ionViewDidEnter() {
    this.storage.get('pop').then((val) => {
      console.log(val);
      if(val=='ok') this.storage.set('pop','');
    });
  }
  ionViewWillEnter() {
    this.objectSubscription = this.afDB.object('/appointments').valueChanges()
      .subscribe(snapShots=>{
        let results = Object.keys(snapShots);
        this.reservation_list = [];
        results.forEach((key,index)=> {
          let appointment = snapShots[key];
          if(appointment.status == 0 && appointment.stylistId == this.currentUserId) {
            this.reservation_list.push(appointment);
          }
        });
      });
    this.objectSubscription1 = this.afDB.object('/users').valueChanges()
      .subscribe(snapShots=>{
        let results = Object.keys(snapShots);

        this.clients = [];
        results.forEach((key,index)=> {
          let user = snapShots[key];
          if(user.type == 'client') {
            this.clients.push(user);
          }
        });
      })
  }

  ionViewWillLeave() {
    this.objectSubscription.unsubscribe();
    this.objectSubscription1.unsubscribe();
  }

  acceptRequest(request,client) {
    let alert = this.alertCtrl.create({
      title: 'Accept',
      message: 'Do you want to accept ' + client.userName + "'s request?",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sure',
          handler: () => {
            console.log('Yes clicked');
            request.status = 1;
            this.afDB.object('/appointments/' + request.appointmentId).update(request);
            this.navCtrl.push(ConfirmationPage, {'request' : request, 'client' : client});
          }
        }
      ]
    });
    alert.present();


  }
  declineRequest(request, client) {
    let alert = this.alertCtrl.create({
      title: 'Decline',
      message: 'Do you want to decline ' + client.userName + "'s request?",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sure',
          handler: () => {
            request.status = 4;
            this.afDB.object('/appointments/' + request.appointmentId).update(request);
            this.toastProvider.presentToast('You declined ' + client.userName + "'s request.",1)
          }
        }
      ]
    });
    alert.present();


  }

  openThread() {
    this.navCtrl.push('ThreadPage');
  }
}
