import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ViewController} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {AngularFireDatabase} from 'angularfire2/database';
import {ToastProvider} from "../../providers/toast/toast";

import * as firebase from 'firebase';
/**
 * Generated class for the ForgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forget',
  templateUrl: 'forget.html',
})
export class ForgetPage {

  objectSubscription : any;
  type : string = '';
  email : string = '';
  users : any;
  qa1 : string;
  qa2 : string;
  qa3 : string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl : ModalController,
              private storage :Storage,
              private toastProvider: ToastProvider,
              private afDB  : AngularFireDatabase) {
    storage.get('type').then((val) => {
      this.type = val;
      console.log(this.type);
    });
  }

  ionViewDidLoad() {
    this.storage.get('email').then((val) => {
      this.email = val;
      console.log(this.email);
    });
  }
  ionViewWillLeave(){

  }
  presentConfirmModal(){
    this.objectSubscription = this.afDB.object(  '/users').valueChanges()
    .subscribe(snapShots=>{

      let results = Object.keys(snapShots);

      results.forEach((key,index)=>{
        let user = snapShots[key];

        console.log(index + " : " + JSON.stringify(user));
        if(user.email == this.email && user.type == this.type){
          if(user.qa1 == this.qa1 && user.qa2 == this.qa2 && user.qa3 == this.qa3){
            firebase.auth().sendPasswordResetEmail(this.email).then(res=>{
              let confirmModal = this.modalCtrl.create('ConfirmPage');
              confirmModal.onWillDismiss(data=>{
                console.log(data);
                this.navCtrl.pop();
              });

              confirmModal.present();
            }, error=>{
              this.toastProvider.presentToast(error.message,2);
            })
          } else {
            this.toastProvider.presentToast('Your security questions are wrong. Please try again.',1);
          }
          return;
        }
        if(index==results.length-1){
          this.toastProvider.presentToast('There is no user with this email as a ' + this.type,1);
        }
      });

      this.objectSubscription.unsubscribe();
    });
  }
  gotoBack() {
    this.navCtrl.pop();
  }
}

