import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastProvider} from "../../providers/toast/toast";
import { AngularFireAuth } from 'angularfire2/auth';
import {AngularFireDatabase} from 'angularfire2/database';
import {User} from "../../providers/modals/modals";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  type : string = '';
  loginForm : FormGroup;
  email : string;
  password : string;
  regUser: any = {};
  objectSubscription : any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private storage: Storage,
              private fb : FormBuilder,
              private afAuth : AngularFireAuth,
              private afDB : AngularFireDatabase,
              private loadingCtrl : LoadingController,
              private toastProvider : ToastProvider) {
    storage.get('type').then((val) => {
      this.type = val;
    });
    this.loginForm = this.buildLoginForm();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  pushToNext() {

    if(this.loginForm.invalid){
      this.toastProvider.presentToast("Please insert valid email and password be of minimum 6 characteres.",1);
      return;
    }
    let loading = this.loadingCtrl.create({
      content: 'Waiting for a moment...',
      dismissOnPageChange: true
    });
    loading.present();
    this.afAuth.auth.signInWithEmailAndPassword(this.email, this.password)
      .then((res)=>{
        loading.dismiss();
        if(!res.user.emailVerified){
          this.toastProvider.presentToast('Please verify your email.',1);
          return;
        }
        this.objectSubscription = this.afDB.object('/users/' + res.user.uid).valueChanges().subscribe(user=>{
          console.log(user);
          this.regUser = user;
          if(this.regUser.type != this.type) {
            this.toastProvider.presentToast('Your type is not matched.',1);
            return;
          }
          this.navCtrl.setRoot('HomePage',{type : this.type});
          this.objectSubscription.unsubscribe();
        }, error=>{
          console.log(error);
        });


      },(err)=>{
        loading.dismiss();
        console.log(err);
        this.toastProvider.presentToast(err.message,2);
      });
  }
  pushToSignup() {
    this.navCtrl.push('PersonalPage');
  }
  pushToForgot() {
    if(!this.loginForm.controls.email.valid){
      this.toastProvider.presentToast("Please insert valid email.",1);
      return;
    }
    this.storage.set('email', this.email);
    this.navCtrl.push('ForgetPage');
  }
  gotoBack() {
    this.navCtrl.pop();
  }

  buildLoginForm() {
    return this.fb.group({
      email: ['', [Validators.required, Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/)]],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6),Validators.maxLength(100)])]  ,
    })
  }


}
