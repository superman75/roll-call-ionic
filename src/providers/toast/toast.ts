import { Injectable } from '@angular/core';
import {ToastController} from "ionic-angular";

/*
  Generated class for the ToastProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ToastProvider {

  constructor(public toastCtrl : ToastController) {
    console.log('Hello ToastProvider Provider');
  }

  public presentToast(text, cssClass : number) {
    let toast;
    console.log('cssClass : ' + cssClass)
    switch (cssClass) {
      case 0:
        toast = this.toastCtrl.create({
          message: text,
          duration: 4000,
          position: 'top',
          cssClass : "toast-success",
          dismissOnPageChange:false
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
        break;
      case 1:
        toast = this.toastCtrl.create({
          message: text,
          duration: 4000,
          position: 'top',
          cssClass : "toast-warning",
          dismissOnPageChange:false
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
        break;
      default:
        toast = this.toastCtrl.create({
          message: text,
          duration: 4000,
          position: 'top',
          cssClass : "toast-danger",
          dismissOnPageChange:false
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
    }

  }

}
