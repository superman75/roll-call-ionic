export class User {
  userId : string;
  userName : string;
  salonName : string;
  email : string;
  qa1 : string;
  qa2 : string;
  qa3 : string;
  profileImg : any;
  porosity : string;
  hairProfile : string;
  hairTyping : string;
  availableStyles : string;
  availableTime : string;
  availableDate:  string;
  paymentToken : string;
  cardLast4Num : string;
  location : string;
  reviewCount : number;
  reviewNum : number;
  type : string;
  open : boolean;
}

export class Appointment {
  appointmentId : string;
  stylistId: string;
  clientId : string;
  availableTime : string;
  availableDate:  string;
  availableStyle : string;
  createdDate : number ;
  card_token : string;
  status : number;
}
