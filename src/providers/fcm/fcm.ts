import { Injectable } from '@angular/core';
import {Firebase} from "@ionic-native/firebase";
import { AngularFireDatabase} from "angularfire2/database";
import {Platform} from "ionic-angular";
/*
  Generated class for the FcmProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FcmProvider {

  constructor(public firebaseNative : Firebase,
              private platform : Platform,
              public afDB : AngularFireDatabase) {
    console.log('Hello FcmProvider Provider');
  }

  // Get permission from the user
  async getToken() {
    let token;
    if(this.platform.is('android')){
      token = await this.firebaseNative.getToken()
    } else if(this.platform.is('ios')){
      token = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
    }
    return this.saveTokenToFireDatabase(token)
  }

  // Save the token to firestore
  private saveTokenToFireDatabase(token) {
    if(!token) return;
    const docData = {
      token,
      userId : 'testUser',
    };
    return this.afDB.object('devices/' + token).set(docData);
  }

  // Listen to incoming FCM messages
  listenToNotifications() {
    return this.firebaseNative.onNotificationOpen();
  }


}
