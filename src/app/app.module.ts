import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import {AngularFireAuthModule} from "angularfire2/auth";
import {AngularFireModule} from "angularfire2";
import {AngularFireDatabaseModule} from "angularfire2/database";

import { MyApp } from './app.component';
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {Stripe} from "@ionic-native/stripe";
import { IonicStorageModule } from '@ionic/storage';

import { ToastProvider } from '../providers/toast/toast';
import {HttpClientModule} from "@angular/common/http";
import {Camera} from "@ionic-native/camera";
import {Geolocation} from "@ionic-native/geolocation";
import { FcmProvider } from '../providers/fcm/fcm';
import {Firebase} from "@ionic-native/firebase";
import { StripeProvider } from '../providers/stripe/stripe';

const firebaseConfig  = {
  apiKey: "AIzaSyD2ValITr-BxImTQW2Ay1u3PNfVGx0NJqk",
  authDomain: "rollcall-eebc2.firebaseapp.com",
  databaseURL: "https://rollcall-eebc2.firebaseio.com",
  projectId: "rollcall-eebc2",
  storageBucket: "rollcall-eebc2.appspot.com",
  messagingSenderId: "589705411629"
};


@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    HttpClientModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    InAppBrowser,
    Stripe,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ToastProvider,
    Camera,
    FcmProvider,
    Firebase,
    StripeProvider
  ]
})
export class AppModule {}
