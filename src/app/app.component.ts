import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import {FcmProvider} from "../providers/fcm/fcm";
import {ToastProvider} from "../providers/toast/toast";
import {tap} from "rxjs/operators";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = 'WelcomePage';

  constructor(platform: Platform, statusBar: StatusBar,
              private toastProvider :ToastProvider,
              private fcm : FcmProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();

      // Get a FCM token
      fcm.getToken();



      // Listen to incoming messages
      fcm.listenToNotifications().pipe(
        tap(msg=>{
          toastProvider.presentToast(msg.body,0);
        })
      )
        .subscribe();

    });

  }

}

